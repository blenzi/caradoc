## Pré-requis

  - Un cluster Kubernetes (K8s);
  - kubectl (https://kubernetes.io/fr/docs/tasks/tools/install-kubectl/);
  - Helm (https://github.com/helm/helm);
  - NFS provisioner (https://github.com/kubernetes-sigs/nfs-subdir-external-provisioner).


## Recettes du Makefile relatives aux charts Helm

```sh

$> make help | grep -i chart

charts-secrets       create helm charts secrets
deploy-ext-charts    deploy all external helm charts
deploy-app-charts    deploy application helm charts (API & Web)
deploy-qdrant        deploy qdrant helm chart
deploy-redis         deploy redis helm chart
deploy-minio         deploy minio helm chart
deploy-mongodb       deploy mongodb helm chart
deploy-postgresql    deploy postgresql helm chart (+ mlflow database creation)
deploy-mlflow        deploy mlflow helm chart
deploy-caradoc-api   deploy caradoc API helm chart
deploy-caradoc-web   deploy caradoc Web helm chart

```


## Déploiement standard

Renseigner les valeurs par défaut pour l'environnement cible dans le fichier Makefile (namespace, ...).


### Créer les secrets K8s

```sh

$> make openai_api_key={MY_OPENAI_API_KEY} charts-secrets

```

### Déployer les charts des composants dont dépend Caradoc

Configurer/ajuster les valeurs des fichiers \*.values.yaml pour l'environnement cible avant les déploiements.

```sh

$> make deploy-ext-charts

```


### Déployer les charts applicatifs (caradoc-api & caradoc-web)

Configurer/ajuster les valeurs (notamment les différents endpoints) des fichiers \*.values.yaml pour l'environnement cible avant les déploiements.

```sh

$> make deploy-app-charts

```


### Exposition de l'application (ingress)

Configurer le fichier \*-caradoc-ingress.yaml (ajout du paramètre **host** par exemple: *caradoc.mydomain.fr*).

```sh

$> make ingress

```

