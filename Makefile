.PHONY: api web base proxy-args charts-secrets deploy-ext-charts deploy-app-charts ingress deploy-qdrant deploy-redis deploy-minio deploy-mongodb deploy-postgresql deploy-mlflow deploy-caradoc-api deploy-caradoc-web docker-compose help
SHELL := /bin/bash

use_proxy = 0
use_ollama = 0
openai_api_key = xxxxxxxx

EMBEDDING_DIM ?= 1024
EMBEDDING_OLLAMA ?=mxbai-embed-large
LLM_OLLAMA ?=mistral

base_api_img_tag = base-r1
api_img_tag = 1.0
web_img_tag = 1.0

k8s_namespace = caradoc
k8s_target_env = dev

HTTP_PROXY ?= $(http_proxy)
HTTPS_PROXY ?= $(https_proxy)

BASE_API_IMG_NAME := dgfip/caradoc-api:${base_api_img_tag}
API_IMG_NAME := dgfip/caradoc-api:${api_img_tag}
WEB_IMG_NAME := dgfip/caradoc-web:${web_img_tag}

MKFILE_PATH := $(abspath $(lastword $(MAKEFILE_LIST)))
MKFILE_DIR := $(dir ${MKFILE_PATH})


define __DEPLOY_CHART__
	@echo "" && \
	echo "deploying $(1) ..." && \
	cd $(2) && \
	helm upgrade --install $(1) $(1)/ --reset-values -f $(strip $(k8s_target_env))-$(strip $(1)).values.yaml --namespace $(k8s_namespace)
endef

define DEPLOY_CHART
	$(call __DEPLOY_CHART__, $(1), "k8s/charts/_external-components_")
endef


api: ## build API docker image
	@cd api && docker build --no-cache -t ${API_IMG_NAME} .


web: ## build Web docker image
	@echo "use_proxy: $(use_proxy)";
ifeq ($(use_proxy), 1)
	@cd web && docker build --no-cache \
	--build-arg http_proxy_arg=$(HTTP_PROXY) \
	--build-arg https_proxy_arg=$(HTTPS_PROXY) \
	-t ${WEB_IMG_NAME} .
else
	@cd web && docker build --no-cache \
	--build-arg http_proxy_arg="" \
	--build-arg https_proxy_arg="" \
	-t ${WEB_IMG_NAME} .
endif


base: ## build API base docker image
	@echo "use_proxy: $(use_proxy)";
ifeq ($(use_proxy), 1)
	@cd api && docker build -f Dockerfile.base \
	--build-arg http_proxy_arg=$(HTTP_PROXY) \
	--build-arg https_proxy_arg=$(HTTPS_PROXY) \
	-t ${BASE_API_IMG_NAME} .
else
	@cd api && docker build -f Dockerfile.base \
	--build-arg http_proxy_arg="" \
	--build-arg https_proxy_arg="" \
	-t ${BASE_API_IMG_NAME} .
endif


proxy-args: ## print used proxy env vars if needed (@see use_proxy)
	@echo "use_proxy: $(use_proxy)"
ifeq ($(use_proxy), 1)
	@echo "HTTP_PROXY: $(HTTP_PROXY)" && \
	echo "HTTPS_PROXY: $(HTTPS_PROXY)";
else
	@echo "no proxy will be used to build the docker images.";
endif


charts-secrets: ## create helm charts secrets
charts-secrets: MONGODB_ROOT_USER=root
charts-secrets: MONGODB_ROOT_PASSWORD::=$(shell uuidgen -r)
charts-secrets:
ifeq ($(openai_api_key), xxxxxxxx)
	@echo "[ WARNING ] openai_api_key is not set ! => 'make openai_api_key=... charts-secrets'" && \
	echo ""
endif
	@kubectl create secret generic redis-secret \
	--namespace $(k8s_namespace) \
	--from-literal db-password=$(shell uuidgen -r) \
	--dry-run=client -o yaml > $(strip $(MKFILE_DIR))redis-secret.yaml \
	&& kubectl apply -f $(strip $(MKFILE_DIR))redis-secret.yaml && \
	\
	kubectl create secret generic minio-secret \
	--namespace $(k8s_namespace) \
	--from-literal minio-root-user=admin \
	--from-literal minio-root-password=$(shell uuidgen -r) \
	--dry-run=client -o yaml > $(strip $(MKFILE_DIR))minio-secret.yaml \
	&& kubectl apply -f $(strip $(MKFILE_DIR))minio-secret.yaml && \
	\
	kubectl create secret generic mongodb-secret \
	--namespace $(k8s_namespace) \
	--from-literal mongodb-root-user=$(MONGODB_ROOT_USER) \
	--from-literal mongodb-root-password=$(MONGODB_ROOT_PASSWORD) \
	--from-literal mongodb-metrics-password=$(shell uuidgen -r) \
	--from-literal mongodb-uri="mongodb://$(strip $(MONGODB_ROOT_USER)):$(strip $(MONGODB_ROOT_PASSWORD))@mongodb:27017/test?serverSelectionTimeoutMS=3000&authSource=admin" \
	--dry-run=client -o yaml > $(strip $(MKFILE_DIR))mongodb-secret.yaml \
	&& kubectl apply -f $(strip $(MKFILE_DIR))mongodb-secret.yaml && \
	\
	kubectl create secret generic postgresql-secret \
	--namespace $(k8s_namespace) \
	--from-literal postgres-password=$(shell uuidgen -r) \
	--from-literal password=$(shell uuidgen -r) \
	--dry-run=client -o yaml > $(strip $(MKFILE_DIR))postgresql-secret.yaml \
	&& kubectl apply -f $(strip $(MKFILE_DIR))postgresql-secret.yaml && \
	\
	kubectl create secret generic mlflow-secret \
	--namespace $(k8s_namespace) \
	--from-literal mlflow-user=admin \
	--from-literal mlflow-password=$(shell uuidgen -r) \
	--dry-run=client -o yaml > $(strip $(MKFILE_DIR))mlflow-secret.yaml \
	&& kubectl apply -f $(strip $(MKFILE_DIR))mlflow-secret.yaml && \
	\
	kubectl create secret generic caradoc-api-secret \
	--namespace $(k8s_namespace) \
	--from-literal openai-api-key=$(strip $(openai_api_key)) \
	--dry-run=client -o yaml > $(strip $(MKFILE_DIR))caradoc-api-secret.yaml \
	&& kubectl apply -f $(strip $(MKFILE_DIR))caradoc-api-secret.yaml


deploy-ext-charts: deploy-qdrant deploy-redis deploy-minio deploy-mongodb deploy-postgresql deploy-mlflow ## deploy all external helm charts

deploy-app-charts: deploy-caradoc-api deploy-caradoc-web ## deploy application helm charts (API & Web)

ingress: ## create ingress to access the application
	@cd k8s && \
	kubectl apply -f $(strip $(k8s_target_env))-caradoc-ingress.yaml

deploy-qdrant: ## deploy qdrant helm chart
	$(call DEPLOY_CHART, qdrant)

deploy-redis: ## deploy redis helm chart
	$(call DEPLOY_CHART, redis)

deploy-minio: ## deploy minio helm chart
	$(call DEPLOY_CHART, minio)

deploy-mongodb: ## deploy mongodb helm chart
	$(call DEPLOY_CHART, mongodb)

deploy-postgresql: POSTGRES_PASSWORD=$(shell kubectl get secret --namespace $(k8s_namespace) postgresql-secret -o jsonpath="{.data.postgres-password}" | base64 --decode) ## deploy postgresql helm chart (+ mlflow database creation)
deploy-postgresql:
	$(call DEPLOY_CHART, postgresql)
	@echo "" && \
	echo "Creating the required database for the mlflow pod ..." && \
	echo "" && \
	sleep 45 && \
	kubectl run postgresql-client --rm --tty -i --restart='Never' \
	--namespace $(k8s_namespace) \
	--image docker.io/bitnami/postgresql:14.3.0-debian-10-r20 \
	--env="PGPASSWORD=$(strip $(POSTGRES_PASSWORD))" \
	--command -- psql -h postgresql -U postgres -d postgres -p 5432 -c "CREATE DATABASE mlflow OWNER postgres;"

deploy-mlflow: ## deploy mlflow helm chart
	$(call DEPLOY_CHART, mlflow)

deploy-caradoc-api: ## deploy caradoc API helm chart
	$(call __DEPLOY_CHART__, caradoc-api, "k8s/charts")

deploy-caradoc-web: ## deploy caradoc Web helm chart
	$(call __DEPLOY_CHART__, caradoc-web, "k8s/charts")


docker-compose: ## launch application with docker-compose
	@echo "use_proxy: $(use_proxy)";
ifeq ($(use_proxy), 1)
	@cd docker_compose && \
	docker compose -f external_components/docker_compose_mlflow.yml build \
	--build-arg http_proxy_arg=$(HTTP_PROXY) \
	--build-arg https_proxy_arg=$(HTTPS_PROXY) && \
	docker compose -f docker_compose_caradoc.yml build \
	--build-arg http_proxy_arg=$(HTTP_PROXY) \
	--build-arg https_proxy_arg=$(HTTPS_PROXY)
else 
	@cd docker_compose && \
	docker compose -f external_components/docker_compose_mlflow.yml build \
	--build-arg http_proxy_arg="" \
	--build-arg https_proxy_arg="" && \
	docker compose -f docker_compose_caradoc.yml build \
	--build-arg http_proxy_arg="" \
	--build-arg https_proxy_arg=""
endif

ifeq ($(use_ollama), 1)
	@ollama serve & \
	ollama pull $(LLM_OLLAMA) & \
	ollama pull $(EMBEDDING_OLLAMA) &
endif

	@cd docker_compose && \
	docker compose -f external_components/docker_compose_mlflow.yml --env-file external_components/config.env up -d && \
	docker compose -f docker_compose_caradoc.yml up -d && \
	sleep 3 && \
	./external_components/init_qdrant.sh $(EMBEDDING_DIM)

help: ## print this help
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z0-9_-]+:.*?## / {gsub("\\\\n",sprintf("\n%22c",""), $$2);printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)
