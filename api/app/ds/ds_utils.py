from typing import Any, List
import os
from llama_index.core.schema import NodeWithScore

from app.config.logger import logger
from app.ds.ai_models import get_embedding_model

def compute_embedding(docs: List[str], model: str) -> List[float]:
    """This function computes embeddings for a given list of texts

    Args:
        docs (List[str]): Input texts to embed
        model (str): Embedding model to used

    Returns:
        (List[float]): Vectors corresponding to docs
    """
    embedding_model = get_embedding_model(client_type=os.getenv("EMBEDDING_CLIENT_TYPE"), model=model)
    return embedding_model.get_text_embedding_batch(texts=docs)


def node_parser(nodes: List[NodeWithScore]) -> str:
    context = ""
    for node in nodes:
        context += node.text + "\n\n"
    return context

