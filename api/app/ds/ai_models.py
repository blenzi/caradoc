from typing import Any, List, Dict
from llama_index.llms.openai import OpenAI
from llama_index.llms.openai_like import OpenAILike
from llama_index.core.bridge.pydantic import PrivateAttr
from llama_index.core.embeddings import BaseEmbedding
from openai import AzureOpenAI, OpenAI as batch_OpenAI
from llama_index.embeddings.openai import OpenAIEmbedding
from llama_index.llms.ollama import Ollama
from llama_index.embeddings.ollama import OllamaEmbedding
import os

OLLAMA_BASE_URL = os.getenv("OLLAMA_BASE_URL")

class CustomOpenAIEmbedding(BaseEmbedding):
    """Embedding model class for LlamaIndex

    Args:
        model_name(str) : name of embedding model
        openai_client : client to connect to openai API (self-hosted model)
    """

    _model_name: str = PrivateAttr()
    _openai_client = PrivateAttr()

    def __init__(
        self,
        openai_client,
        model_name: str,
        **kwargs: Any,
    ) -> None:
        self._model_name = model_name
        self._openai_client = openai_client
        super().__init__(**kwargs)

    @classmethod
    def class_name(cls) -> str:
        return "CustomOpenAIEmbedding"

    async def _aget_query_embedding(self, query: str) -> List[float]:
        return self._get_query_embedding(query)

    def _get_query_embedding(self, query: str) -> List[float]:
        embeddings = self._openai_client.embeddings.create(
            input=query, model=self._model_name  # model = "deployment_name".
        )
        return embeddings.data[0].embedding

    def _get_text_embedding(self, text: str) -> List[float]:
        embeddings = self._openai_client.embeddings.create(
            input=text, model=self._model_name  # model = "deployment_name".
        )
        return embeddings.data[0].embedding

    def _get_text_embeddings(self, texts: List[str]) -> List[List[float]]:
        embeddings = self._openai_client.embeddings.create(
            input=texts, model=self._model_name  # model = "deployment_name".
        )
        embs = [e.embedding for e in embeddings.data]
        return embs

    async def _aget_query_embedding(self, query: str) -> List[float]:
        return self._get_query_embedding(query)

    async def _aget_text_embedding(self, text: str) -> List[float]:
        return self._get_text_embedding(text)



def get_llm_model(client_type : str, model: str, max_tokens : int, temperature : float, top_p : float):
    """Get llm model according to client type and parameters

    Args:
        client_type (str): Client type e.g openai, custom
        model (str): LLM's name
        max_tokens (int): max tokens parameter for llm completion
        temperature (float): temperature parameter for llm completion
        top_p (float): top_p parameter for llm completion

    Returns:
         LLM llamaindex model
    """
    if client_type == "custom" :
        return OpenAILike(model=model, max_tokens=max_tokens, temperature=temperature, top_p=top_p, timeout=600)
    elif client_type == "openai":
        return OpenAI(model=model, max_tokens=max_tokens, temperature=temperature, top_p=top_p, timeout=600)
    elif client_type == "ollama":
        return Ollama(model="mistral", base_url = OLLAMA_BASE_URL, max_tokens=max_tokens, temperature=temperature, top_p=top_p, request_timeout=120.0)
    else :
        return "Please provide a right client type"
    
def get_embedding_model(client_type : str, model: str):
    """Get embedding model according to a client type 

    Args:
        client_type (str): Client type e.g openai, custom
        model (str): embedding model's name

    Returns:
         Embedding llamaindex model
    """
    if client_type == "custom" :
        openai_client = AzureOpenAI(
            api_key=os.environ.get("OPENAI_API_KEY"),
            azure_endpoint=os.environ.get("OPENAI_API_BASE"),
            api_version=os.environ.get("OPENAI_API_VERSION"),
        )
        return CustomOpenAIEmbedding(model_name=model, openai_client=openai_client)
    elif client_type == "openai":
        return OpenAIEmbedding(model=model, timeout=60)
    elif client_type == "ollama":
        return OllamaEmbedding(
                model_name=model,
                base_url=OLLAMA_BASE_URL,
                ollama_additional_kwargs={"mirostat": 0},
                )
    else :
        return "Please provide a right client type"
    
def llm_batch_inference(prompts : List[str], client_type : str, model : str, max_tokens : int, temperature : float, top_p : float) -> List[str] | str:
    """ Batch inference LLM according to client type

    Args:
        prompts (List[str]): inputs for LLM
        model (str): LLM's name
        max_tokens (int): max tokens parameter for llm inference
        temperature (float): temperature parameter for llm inference
        top_p (float): top_p parameter for llm inference


    Returns:
        List[str] | str : Outpus from LLM batch inference
    """
    if client_type == "custom":
        openai_client = AzureOpenAI(
            api_key=os.environ.get("OPENAI_API_KEY"),
            azure_endpoint=os.environ.get("OPENAI_API_BASE"),
            api_version=os.environ.get("OPENAI_API_VERSION"),
        ) 
        return [t.text for t in openai_client.completions.create(
        prompt=prompts, model=model, max_tokens=max_tokens, temperature=temperature, top_p=top_p
    ).choices]
    elif client_type == "openai" :
        openai_client = batch_OpenAI(api_key=os.environ.get("OPENAI_API_KEY"))
        return [t.text for t in openai_client.completions.create(
        prompt=prompts, model=model, max_tokens=max_tokens, temperature=temperature, top_p=top_p
    ).choices]
    elif client_type == "ollama" :
        llm = get_llm_model(client_type=client_type, model=model,max_tokens=max_tokens, temperature=temperature, top_p=top_p )
        output = []
        for prompt in prompts :
            output.append(llm.complete(prompt).text)
        return output
    else :
        return "Please provide a right client type"