import json
import os

MODELS = json.load(open(os.getenv("MODELS_CONFIG_PATH")))
PRECISION = os.getenv("RAG_PRECISION")
