import os

from redis import Redis

client = Redis(
    host=os.getenv("REDIS_HOST"),
    port=os.getenv("REDIS_PORT"),
    password=os.getenv("REDIS_PASSWORD"),
    decode_responses=True,
)

if os.getenv("REDIS_PORT") and os.getenv("REDIS_HOST") and os.getenv("REDIS_PASSWORD"):
    client = Redis(
        host=os.getenv("REDIS_HOST"),
        port=os.getenv("REDIS_PORT"),
        password=os.getenv("REDIS_PASSWORD"),
        decode_responses=True,
    )
elif os.getenv("REDIS_HOST"):
    client = Redis(
        host=os.getenv("REDIS_HOST"),
        decode_responses=True,
    )
else:
    client = Redis(
        host="redis-service",
        decode_responses=True,
    )
