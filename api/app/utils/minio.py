import asyncio
import os

from fastapi import HTTPException, UploadFile
from minio.deleteobjects import DeleteObject
from starlette import status
from starlette.responses import FileResponse

from app.config.logger import logger
from app.config.minio import COLLECTIONS_BUCKET_NAME
from app.config.minio import client as minio_client

token_pattern = r"^[a-zA-Z0-9]{16}-[a-zA-Z0-9]{16}$"


def remove_files_from_bucket(bucket_name: str, prefix: str):
    """Remove files from a bucket with a specific prefix

    Args:
        bucket_name (str): The bucket name
        prefix (str): The prefix to remove

    Returns:
        None

    """

    # We prepare our list of objects to remove
    files_to_delete = map(
        lambda x: DeleteObject(x.object_name),
        minio_client.list_objects(bucket_name, prefix, recursive=True),
    )

    # We proceed to files removal, and retrieve any errors
    errors = minio_client.remove_objects(bucket_name, files_to_delete)
    for error in errors:
        print("An error occurred whi deleting object", error)


def upload_file_to_bucket(bucket_name: str, object_name: str, file: UploadFile):
    """Upload a file to the bucket

    Args:
        bucket_name (str): The bucket name
        object_name (str): The object name
        file (UploadFile): The file to upload

    Returns:
        None

    """

    logger.debug(f"Uploading file {object_name} to bucket {bucket_name}")
    minio_client.put_object(
        bucket_name,
        object_name,
        file.file,
        file.size,
        file.content_type,
    )


async def upload_file_to_bucket_async(object_name: str, file: UploadFile):
    """Uploads a file to the bucket | async version

    Args:
        object_name (str): The object name
        file (UploadFile): The file to upload

    Returns:
        None
    """
    await asyncio.to_thread(
        upload_file_to_bucket, COLLECTIONS_BUCKET_NAME, object_name, file
    )


async def get_file_from_bucket_async(object_name: str, output_file_path: str):
    """Retrieve a file from minio | async version

    Args:
        object_name (str): The object name
        output_file_path (str): The output file path

    Returns:
        None

    """
    await asyncio.to_thread(
        minio_client.fget_object, COLLECTIONS_BUCKET_NAME, object_name, output_file_path
    )


async def download_file_from_bucket(
    index: str, file_id: str, filename: str, object_name: str
):
    """Downloads a file from minio

    Args:
        index (str): The collection index
        file_id (str): The file id
        filename (str): The filename
        object_name (str): The object name

    Returns:
        FileResponse: The file response

    Raises:
        HTTPException

    """

    # We retrieve the file from minio and store in it a temporary file
    try:
        # Let's store the file in a temporary location
        tmp_file_path = f"/tmp/{index}-{file_id}"

        # We retrieve the file from minio
        await get_file_from_bucket_async(object_name, tmp_file_path)

    except HTTPException:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Error while retrieving file {file_id} from collection {index} from minio",
        )

    # We check if the file has been correctly created
    if not os.path.exists(tmp_file_path):
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Unable to create file (collection_id: {index}, file_id: {file_id}) for download",
        )

    # We serve the file to the client
    return FileResponse(
        tmp_file_path, media_type="application/octet-stream", filename=filename
    )
