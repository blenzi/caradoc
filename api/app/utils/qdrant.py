import asyncio

from fastapi import UploadFile
from qdrant_client.http import models

from app.config.qdrant import BASE_COLLECTION_NAME
from app.config.qdrant import client as qdrant_client
from app.config.rag import MODELS
from app.ds.parsing_loading_utils import ingest_data


def create_qdrant_collection_index(index: str):
    """Creates a new index in the qdrant collection

    Args:
        index (str): The index to create

    Returns:
        None
    """
    qdrant_client.create_payload_index(
        collection_name=BASE_COLLECTION_NAME,
        field_name=index,
        field_schema=models.PayloadSchemaType.KEYWORD,
    )


def ingest_file(index: str, file: UploadFile, file_id, preprocessed: bool = False):
    """Ingest a file into the Qdrant vector store

    Args:
        index (str): User token
        file (UploadFile): File to ingest
        file_id (str): File ID
        preprocessed (bool, optional): Whether the file is already preprocessed

    Returns:
        None

    """

    ingest_data(
        filename=file.filename,
        file_id=file_id,
        data=file.file,
        index=index,
        embedding_model=MODELS["embed_model"],
        fiab_model=MODELS["fiab_llm_model"],
        apply_fiab=True,
        preprocessed=preprocessed,
    )


async def ingest_file_async(
    index: str, file: UploadFile, file_id: str, preprocessed: bool
):
    """Ingests a file into qdrant collection | async version

    Args:
        index(str): The collection id
        file (UploadFile): The file to ingest
        file_id (str): The file id
        preprocessed (bool): Whether the file is already preprocessed

    Returns:
        None

    """
    await asyncio.to_thread(ingest_file, index, file, file_id, preprocessed)


def remove_qdrant_data(index: str, file_id: str | None = None):
    """Remove data from qdrant whether index or files

    Args:
        index (str): User token
        file_id (str, optional): File ID
    """
    must = [
        models.FieldCondition(
            key="index",
            match=models.MatchValue(value=str(index)),
        ),
    ]

    if file_id is not None:
        must.append(
            models.FieldCondition(
                key="file_id",
                match=models.MatchValue(value=str(file_id)),
            )
        )

    qdrant_client.delete(
        collection_name=BASE_COLLECTION_NAME,
        points_selector=models.FilterSelector(
            filter=models.Filter(
                must=must,
            )
        ),
    )


async def remove_qdrant_data_async(index: str, file_id: str | None = None):
    """Remove data from qdrant | async version

    Args:
        index (str): The index to remove
        file_id (str, optional): The file id to remove

    Returns:
        None
    """
    await asyncio.to_thread(remove_qdrant_data, index, file_id)
