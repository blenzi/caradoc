import os
import traceback
from contextlib import asynccontextmanager

import nltk
from fastapi import FastAPI

from app.config.logger import logger as custom_logger
from app.config.mongo import client as mongo_client
from app.config.mongo import init as init_mongo

from .dependencies.ai_models import init_eval_message_type_model
from .exceptions.custom_exception import CustomException
from .routers import chat, collections, evaluation, settings

if os.environ.get("HTTP_PROXY"):
    nltk.set_proxy(os.environ.get("HTTP_PROXY"))


@asynccontextmanager
async def lifespan(app: FastAPI):
    # Connect to mongo
    await init_mongo()

    # Load the ML model
    init_eval_message_type_model(model_path="ai_models/clf_pr.skops")

    yield

    mongo_client.close()


def create_app() -> FastAPI:
    """Returns an FastAPI app with a custom logger

    Returns:
        FastAPI: _description_
    """
    app = FastAPI(lifespan=lifespan, debug=False)
    app.logger = custom_logger
    app.include_router(settings.router)
    app.include_router(chat.router)
    app.include_router(collections.router)
    app.include_router(evaluation.router)

    return app


app = create_app()


@app.exception_handler(CustomException)
async def custom_exception_handler(_, exception: CustomException):
    """Log the original exception (error message + stack trace) and re-raise it

    Args:
        exception (CustomException): The custom exception object

    Raises:
        Exception: The original exception that occurred
    """
    # We log the intermediary exception message if present
    if exception.args[0] != "CustomException occurred":
        custom_logger.error(exception)

    # We log the original exception message and stack trace
    custom_logger.error(exception.original_exception)
    custom_logger.error(traceback.format_exc())

    raise exception.original_exception
