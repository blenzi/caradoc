from typing import Tuple

from fastapi import APIRouter, HTTPException
from starlette import status

from ..ds.rag_pipeline import workflows
from ..exceptions.custom_exception import CustomException
from ..models.app.success_response import SuccessResponse

router = APIRouter(
    prefix="/settings",
    tags=["settings"],
)


@router.get(
    "/workflows",
    response_description="Get the list of available workflows",
    response_model=SuccessResponse[Tuple[str, ...]],
)
async def get():
    """Return the list of available workflows

    Returns:
        SuccessResponse[Tuple[str, ...]]: List of available workflows

    Raises:
        CustomException
    """
    try:
        return SuccessResponse(data=workflows)
    except Exception:
        raise CustomException(
            original_exception=HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=f"Error while fetching workflows",
            )
        )
