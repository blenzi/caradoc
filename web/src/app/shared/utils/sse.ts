/**
 * This helper function encapsulates the logic to process and handle SSE event-streams
 * @param response
 * @param processCallback
 * @param doneCallback
 * @param separator
 */
const handleSSE = async (
    response: Response,
    processCallback: (currentEvent: any) => void,
    doneCallback: () => void,
    separator: string = "\n"
) => {
    if (response.body) {
        const reader = response.body.getReader()
        const decoder = new TextDecoder("utf-8")

        // As long as there is data returned by the stream, we process the incoming messages
        while (true) {
            const {value, done} = await reader.read();
            const currentEventStr = decoder.decode(value)
            if (done) {
                doneCallback()
                break;
            }

            // Since we receive JSON data, we need to parse it to exploit it
            try {
                // It seems events sometime arrives together as being buffered
                const currentEvents = currentEventStr
                    .split(separator)
                    .filter(eventStr => eventStr)
                for (let currentEventStr of currentEvents) {
                    const currentEvent = JSON.parse(currentEventStr)
                    processCallback(currentEvent)
                }
            } catch (e) {
                throw new Error("La traitement de la réponse a échoué.")
            }
        }
    } else {
        throw new Error("La génération de réponse a échoué.")
    }
}

export { handleSSE }