<div style="display: flex; width: 100%">
  <img src="doc/logo-caradoc.png" alt="logo caradoc" width="150" style="margin: auto"/>
</div>

# CARADOC : Chat d'Assistance à la Recherche Augmentée DOCumentaire

[![License: MIT](doc/license-mit.svg)](https://opensource.org/licenses/MIT)

![Capture Caradoc](doc/capture-caradoc.png)

📋 Dans un contexte où l'information textuelle est omniprésente, il est important pour les administrations de s'appuyer
sur les dernières avancées technologiques en termes d'IA et notamment d'IA générative pour développer un outil
performant, sécurisé pour assister les agents dans leur quotidien. C'est dans cette perspective que la Délégation à la
Transformation Numérique (DTNum) de la DGFiP a développé CARADOC, un moteur de recherche « intelligent » permettant de
faciliter l'accès à l'information.

🔍 Cet outil innovant propose deux modes de fonctionnement distincts afin de répondre au maximum de cas d'usage :

- Un mode **« fichier »** permettant d'interagir de manière sécurisée avec des documents sensibles. Dans ce mode, aucune
  donnée n'est partagée en dehors de l'échange permettant une interaction en toute sécurité.

- Un mode **« collection »** permettant d'interagir avec des bases de données personnalisées et partageables entre
  collaborateurs. Cette version est plus adaptée pour de larges corpus de documents et embarque un système complet de
  gestion de base de données de fichiers.

⚒️ Par ailleurs, l'application peut être considérée comme les fondations de divers projets d'IA générative autour des
méthodes dites de RAG (Retrieval Augmented Generation) puisqu'elle fournit de l'ingestion des données jusqu'à l'
évaluation des diverses méthodes. Soucieuse de proposer une expérience accessible à tous les profils d'utilisateurs (
suivant le Référentiel général d'amélioration de l'accessibilité - RGAA) et aux standards graphiques de l'État (suivant
le Système de Design de l'État), l'équipe de développement de cet outil a pu travailler avec les designers experts de la
DTNum sur ces sujets.

🚀 L'application CARADOC est la première d'une suite d'outils d'IA générative développée par la DGFIP pour assister les
agents dans leur quotidien à travers une approche responsable et partagée en ouvrant son code source à la communauté.

## 🧑‍💻 Installation

### Architecture technique

![Architecture technique](doc/AYD_architecture.drawio.png)

Afin d'avoir une interface de démo facilement déployable, l'application a été « packagée » dans un fichier docker
compose. Celui-ci comprend :

- l'API
- l'interface web
- un serveur nginx
- un service redis

Cependant, pour que l'application fonctionne, des composants externes sont nécessaires :

- une base de données Qdrant
- une base de données MongoDB
- un stockage objet Minio S3
- un service d'API LLM sous format OpenAI (AzureOpenAI ou OpenAI)

Dans notre cas, en dehors du service LLM, tous les services externes sont fournis dans des fichiers docker compose
externes.

### Pré-requis

Pour faire fonctionner l'application, il vous faudra :

- docker et docker compose ou un cluster kubernetes avec Helm
- un service LLM (OpenAI)

### 1. Images docker

Dans notre application, nous retrouvons principalement deux images :

- dgfip/caradoc-api
- dgfip/caradoc-web

Le Makefile du dépôt permet de construire les images Docker:
```bash

$> make help

api                  build API docker image
web                  build Web docker image
base                 build API base docker image
proxy-args           print used proxy env vars if needed (@see use_proxy)
charts-secrets       create helm charts secrets
deploy-ext-charts    deploy all external helm charts
deploy-app-charts    deploy application helm charts (API & Web)
ingress              create ingress to access the application
deploy-qdrant        deploy qdrant helm chart
deploy-redis         deploy redis helm chart
deploy-minio         deploy minio helm chart
deploy-mongodb       deploy mongodb helm chart
deploy-postgresql    deploy postgresql helm chart (+ mlflow database creation)
deploy-mlflow        deploy mlflow helm chart
deploy-caradoc-api   deploy caradoc API helm chart
deploy-caradoc-web   deploy caradoc Web helm chart
docker-compose       launch application with docker-compose
help                 print this help

```

Pour build l'image Web :

```bash
make web
```

Pour build l'image API :

```bash

make base
# make use_proxy=1 base

make api

```

### 2. Installation via Docker compose

Dans un premier temps, il faut configurer les services externes i.e Minio, Qdrant et Mlflow.

L'installation de Minio/Mlflow se fait à l'aide de la commande suivante :

```bash
docker compose -f external_components/docker_compose_mlflow.yml build  --build-arg http_proxy_arg=""  --build-arg https_proxy_arg=""
docker compose -f external_components/docker_compose_mlflow.yml --env-file external_components/config.env up -d
```

Ensuite, nous allons paramétrer le Minio en créant deux buckets, une access key et secret key à renseigner dans les
variables d'environnements du fichier config.env.

Les buckets s'appelleront :

- mlflow
- caradoc

Ensuite, il sera possible de lancer le Minio et le MLFlow.

Par la suite, pour lancer l'application, il suffit de lancer le script suivant :

```bash
make docker-compose EMBEDDING_DIM=1024
# make docker-compose  use_ollama=1 LLM_OLLAMA=mistral EMBEDDING_OLLAMA=mxbai-embed-large EMBEDDING_DIM=1024

```
Cela lancera notre application ainsi que le script permettant de créer une collection dans qdrant au nom : `caradoc`

NB : Il est possible de lancer l'application avec un serveur ollama directement avec le flag use_ollama=1

Le choix du modèle est disponible à l'aide des deux arguments : LLM_OLLAMA et EMBEDDING_OLLAMA

Les modèles disponibles sont dans [la documentation d'Ollama](https://ollama.com/library) 

### 3. Installation via Helm
Charts Helm [ici](./k8s/charts/README.md)


### ⚙️ Configuration

#### Variables d'environnement

Dans les différents modes de déploiement, des variables d'environnement sont fournies pour fournir un paramétrage avancé
de l'application.

Le fichier `docker-compose` sert d'interface de paramétrage dans lequel on vient renseigner les variables
d'environnement suivantes :

- `OPENAI_API_BASE` : URL serveur OpenAI (ou Azure Endpoint)
- `OPENAI_API_KEY` : Clé API OpenAI
- `QDRANT_ENDPOINT` : URL Base de données Qdrant
- `QDRANT_BASE_COLLECTION_NAME` : Nom de la collection mère Qdrant
- `RAG_PRECISION` : Nombre de documents retournés pour les appels RAG
- `MINIO_ENDPOINT` : URL stockage objet Minio
- `MINIO_ACCESS_KEY` : Clé d'accès stockage objet Minio
- `MINIO_SECRET_KEY` : Clé secrète stockage objet Minio
- `MONGODB_URI` : URI de la base Mongo
- `MONGO_DATABASE_NAME` : Nom de la base de données pour intégrer les feedbacks utilisateurs
- `MONGO_USERNAME` : Utilisateur Mongo
- `MONGO_PASSWORD` : Mot de passe Mongo
- `MODELS_CONFIG_PATH` : Chemin du fichier spécifiant les noms des modèles
- `PROMPT_FILE_PATH` : Chemin vers le fichier contenant les prompts de l'application.
- `MLFLOW_URI` : URI du service MLflow
- `LOGGER_PATH` : Chemin pour les fichiers de logs
- `LOGGER_LEVEL` : Niveau de logs
- `REDIS_HOST` (optionnel) : URL du service redis
- `REDIS_PORT` (optionnel) : Port du service redis
- `REDIS_PASSWORD`(optionnel) : Mot de passe de la base redis
- `LLM_CLIENT_TYPE` : Type de client à utiliser pour gérer les llms : custom -> OpenAILike, openai
- `EMBEDDIN_CLIENT_TYPE` : Type de client à utiliser pour gérer les modèles d'embeddings : custom -> OpenAILike, openai
- `OLLAMA_BASE_URL`: URL API Ollama

#### Fichier `prompt.yaml`

Ce fichier est un fichier permettant de gérer de manière séparée les prompts liés à l'application.

Il est structuré de la manière suivante :

```yml
[task]
    [subtask]
        [model]
            prompt :
            temperature :
            top_p :
            max_tokens :
            comment :

```

NB : Dès lors qu'un nouveau modèle est ajouté, il est important d'ajouter les prompts adéquats. 

#### Fichier `models_config.json`

Ce fichier de configuration permet de paramétrer les modèles utilisés dans l'application. Il permet aussi d'ajouter des
nouveaux noms de modèles lors de la création de pipelines plus complexes.

## 💁 Contribuer

En tant que projet open source dans un domaine en constante évolution, nous sommes particulièrement ouverts aux
contributions, que ce soit sous la forme d'une nouvelle fonctionnalité, d'optimisation ou encore d'une meilleure
documentation.

Pour plus d'informations sur la méthode de contribution, voir [ici](doc/CONTRIBUTING.md).

## 📖 Documentation technique

### Intégration LLM disponible

`CARADOC` supporte actuellement les serveurs de type OpenAI (Azure également), Ollama
Pour un test rapide, une unique clé OpenAI suffira, sinon vous pourrez proposer l'URL de votre API LLM (ou Azure
Endpoint) avec la clé associée.

Pour plus d'informations sur la configuration [Ollama](doc/Ollama.md)

Pour intégrer de nouveaux types de clients, il suffira de modifier le fichier `api/app/ds/ai_models` en ajoutant les clients dans les fonctions :
- `get_llm_model`
- `get_embedding_model`
- `llm_batch_inference`

### ⚡ Pipeline disponible

L'application utilise [LlamaIndex](https://github.com/run-llama/llama_index) pour la création de ses pipelines.
Cependant, une couche d'abstraction supplémentaire est offerte afin de permettre aux utilisateurs et développeurs
d'imaginer des pipelines innovants de la manière dont ils le souhaitent. Cela permet à l'application de ne pas dépendre
complétement du framework initialement choisi.

Cette couche d'abstraction impose les méthodes suivantes :

- `query` : méthode pour faire des requêtes complètes sur le pipeline RAG
- `retrieve` : méthode pour récupérer les documents pertinents à partir d'une requête

Ces classes se définissent dans le fichier `/api/app/ds/rag_pipeline`. Il faudra veiller à bien ajouter le nouveau nom
du pipeline dans le tuple `worflows` situé en tête de fichier.

#### 1. Pipeline RAG "Classique"

![Naive RAG](doc/Naive_RAG.png)

Ce dernier repose sur le principe du RAG (Retrieval Augmented Generation) issu du
papier ["Retrieval-Augmented Generation for Knowledge-Intensive NLP Tasks"](https://arxiv.org/abs/2005.11401)

Cela permet d'avoir une utilisation simple de l'application.

#### 2. Pipeline RAG **"Checker"**

![Check RAG](doc/Check_RAG.png)

Dans ce cas, nous récupérons le pipeline initialement conçu avec l'ajout d'un contrôle de cohérence sur la sortie du
retriever. Un LLM vérifie que dans les documents retournés, il existe les informations nécessaires à l'élaboration de la
réponse.

### 🔄 Ingestion des documents

![Ingestion](doc/ingestion_data.png)

L'ingestion est séparée en deux parties :

- Ingestion du fichier brut
- Ingestion "vectorielle"

L'ingestion du fichier brut se fait dans un stockage objet Minio permettant de sauvegarder les fichiers bruts
nécessaires pour des fonctionnalités comme le téléchargement.

L'ingestion vectorielle est plus complexe puisqu'elle demande différents traitements.

#### 1. Parsing

La première étape consiste à récupérer les informations textuelles des documents. Ainsi, nous réalisons un traitement
conditionnel en fonction du type de fichier.

Si le document est un PDF, nous le convertirons en premier lieu en fichier HTML à l'aide de `PDFMiner`, ce qui permet de
garder la structure de celui-ci. Dans un second temps, un parsing est réalisé à l'aide du module Unstructured.

Pour les autres types de fichiers supportés, le parsing est réalisé via le
module [Unstrucutred](https://docs.unstructured.io/welcome).

Ce dernier assure aussi le chunking à l'aide de la structure du fichier (à l'aide des titres notamment).

#### 2. Fiabilisation

Lors de cette étape, l'objectif est de fiabiliser notre document si besoin.

La condition de fiabilisation est régie par un filtre selon le pourcentage de mots directement disponibles dans le
dictionnaire. Si ce dernier est inférieur à un seuil défini dans le code de l'application (0.8), alors l'étape de
fiabilisation est appliquée.

Cette étape consiste à utiliser un LLM pour fiabiliser le document et restructurer certaines parties.

#### 3. Vectorisation et injection

À l'aide d'un modèle d'embedding, les chunks du document sont vectorisés et injectés dans une base de données
vectorielle Qdrant.

#### Modification du pipeline d'ingestion

Pour modifier ce pipeline, il suffit de modifier la fonction `ingest_data` du
fichier `/api/app/ds/parsing_loading_utils.py`.

### 📝 Évaluation du pipeline

![Evaluation](doc/eval_pipeline.drawio.png)

#### Principe

Tout comme pour les pipelines de RAG, une couche d'abstraction a été imaginée afin de permettre aux contributeurs
d'ajouter leur méthode d'évaluation en fonction de leur cas d'usage.

Cette abstraction comprend uniquement la méthode `eval_pipeline` utilisée dans le endpoint
d'évaluation : `/evaluation/eval`. Il est possible de créer un nouveau pipeline dans le
fichier `/api/app/ds/eval_pipeline` ou de compléter celui initialement présent.

#### Solution proposée

Cette fonctionnalité vise à fournir aux utilisateurs et administrateurs de l'application un framework d'évaluation des
pipelines de RAG sur une base de données.

Ce framework propose trois grandes métriques :

- **Indice de performance du moteur de recherche** : Cette métrique évalue la performance de la partie moteur de
  recherche ;
- **Indice de confiance** : Cette métrique permet d'obtenir une métrique sur l'évaluation des modèles ;
- **Qualité de réponse** : Cette métrique supervisée vérifie si la méthode de RAG renvoie la bonne réponse à une
  question donnée ;

Toutes ces métriques sont basées sur la méthode du LLM juge présentée dans le
papier : [Judging LLM-as-a-Judge with MT-Bench and Chatbot Arena](https://arxiv.org/abs/2306.05685)

De plus, le jeu d'évaluation est généré automatiquement à l'aide d'un LLM. Cela permet de pouvoir obtenir un jeu
rapidement sur des bases de données utilisateurs spécifiques. En effet, chaque cas d'usage a des besoins différents qui
peuvent être mieux répondus à l'aide de certains pipelines.

NB : Bien que critiqué, la méthode "LLM-as-a-Judge" est la seule méthode "sans labellisation" permettant d'évaluer les
sorties des méthodes de RAG. Ainsi les métriques sont dépendantes des modèles LLM juges utilisés et peuvent comportés un
bruit important. Il est donc recommandé d'utiliser le LLM le plus performant pour executer ce genre de tâche tout en
respectant vos normes de sécurité et de confidentialité

#### Indice de performance du moteur de recherche

Cet indice est évalué en deux étapes :

- **Evaluation classique** : Pour une requête associée à un document donné, nous évaluons la capacité du moteur de
  recherche à récupérer le document associé à la requête selon une précision k. En effet, la recherche par similarité
  est
  configuré de manière à retourner les k-documents les plus proches
- **Evaluation LLM** : Pour une requête, nous évaluons la capacité du moteur de recherche à récupérer l'information
  permettant de répondre à la requête, quel que soit le document. Ce mode d'évaluation permet de s'affranchir de
  l'indépendance d'une information dans un corpus de documents. Une requête peut être répondue à l'aide d'un ou
  plusieurs documents

#### Indice de confiance

Cet indice est calculé en utilisant une méthode en plusieurs étapes :

En premier lieu, il faut pouvoir catégoriser notre message :

Pour pouvoir correctement répondre à la question : « Est-ce que ma méthode de RAG a halluciné ? » il faut tout d'abord
répondre à la question : « Est-ce que ma méthode RAG sait détecter quand l'information n'est pas présente dans le
contexte ? »

En effet, en sortie, il peut y avoir deux types de réponses :

- Une réponse normale : Quelle est la taille de la Tour Eiffel ? La taille de la Tour Eiffel est de 300 m.
- Un contrôle de réponse : Quelle est la taille de la Tour Eiffel ? Désolé, je n'ai pas d'informations dans le contexte
  fourni pour répondre à la question : Quelle est la taille de la Tour Eiffel.

Ainsi, à l'aide d'un classifieur préalablement entraîné sur des données synthétiques, il est possible de classifier avec
grande précision les deux types de réponses.

Dans le cas d'une réponse normale, la seconde étape consiste à évaluer avec un LLM le degré d'hallucination de notre
réponse. Dans l'autre cas, si l'information est présente dans le contexte, alors notre méthode de RAG a halluciné, dans
le cas contraire, elle a réalisé un autocontrôle correct.

L'intérêt de cette méthode réside dans sa dissociation des cas pour écarter certains cas particuliers et fournir
l'évaluation la plus pertinente possible.

#### Qualité de réponse

Cet indice est évalué de manière supervisée : à partir de sa question et sa réponse associée, nous évaluons la
pertinence de la réponse fournie par le pipeline par rapport à la question et réponse de référence.

## Auteurs

- Hugo SIMON @hugosmn : Pilote du projet / Data scientist (hugo.simon@dgfip.finances.gouv.fr, simon.hugo59@orange.fr)
- Mamadou DIALLO : Lead Fullstack Developer (mamadou.diallo-consultant@dgfip.finances.gouv.fr, devmadou@gmail.com)
- Johann DUTON-MORGAND : Architecte DevOps
- Camille BRIER : Data scientist
- Thomas BINDER : Chef de l'équipe Data science DTNUM
- Hermann WOEHREL : Data scientist
- Raphaël ROZENBERG : Data scientist
- Mehdi EL YAAKABI : Data scienist
